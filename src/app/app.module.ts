import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { AppComponent } from './app.component';
import { HeaderComponent } from './app/header/header.component';
import { ProjectDetailsComponent } from './app/header/project-details/project-details.component';
import { ProjectPropertiesComponent } from './app/header/project-properties/project-properties.component';
import { AppService } from './app/header/header.appservice';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProjectDetailsComponent,
    ProjectPropertiesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
