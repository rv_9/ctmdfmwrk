import { Component, OnInit } from '@angular/core';
import { AppService } from '../header.appservice';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {
  create = false;
  open = false;
  saveclick = false;
  
  constructor(private appService : AppService) { }

  ngOnInit() {
  }

  onCreate()  {
    this.open = false;
    this.create = true;
  }

  onOpen(){
    this.create = false;
    this.open = true;
  }

  onClick() {
    console.log("projectdetailsts")
    this.appService.showProperties.emit(true);
  }

}
